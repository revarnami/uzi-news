How to run project

1. Create secrets.properties file in root project folder
2. Add API_KEY = "Apl1h6hAgcG0Aaet2oNBarIZjGeQZLGh" in secrets.properties file
3. If you want to create your own API_KEY go to https://developer.nytimes.com/accounts/create
4. After adding API_KEY try running the project and enjoy it.
