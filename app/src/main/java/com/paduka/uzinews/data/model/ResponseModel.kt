package com.paduka.uzinews.data.model

/*
 Created by Fauzi Arnami on 3/16/18.
*/

class ResponseModel(
    val docs: List<ArticleModel>? = null
)