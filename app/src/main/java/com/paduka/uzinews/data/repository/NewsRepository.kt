package com.paduka.uzinews.data.repository

import com.paduka.uzinews.domain.Article
import io.reactivex.Single

interface NewsRepository {
    fun getTopHeadlines(): Single<List<Article>>
    fun getEverything(query: String, page: Int, apiKey: String): Single<List<Article>>
}