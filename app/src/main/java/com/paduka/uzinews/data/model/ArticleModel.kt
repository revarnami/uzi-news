package com.paduka.uzinews.data.model

data class ArticleModel(
    val headline: HeadlineModel? = null,
    val snippet: String? = null,
    val pub_date: String? = null,
    val byline: ByLineModel? = null,
    val web_url: String? = null,
    val lead_paragraph: String? = null,
    val multimedia: List<MultimediaModel>? = null
)