package com.paduka.uzinews.data.response

import com.paduka.uzinews.data.model.ResponseModel

data class TopHeadlineResponse(val status: String, val response: ResponseModel)