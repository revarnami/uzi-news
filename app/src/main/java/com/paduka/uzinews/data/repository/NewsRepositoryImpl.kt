package com.paduka.uzinews.data.repository

import com.paduka.uzinews.data.database.dao.ArticleDao
import com.paduka.uzinews.data.mapper.ArticleMapper
import com.paduka.uzinews.data.services.NewsServices
import com.paduka.uzinews.domain.Article
import io.reactivex.Single

open class NewsRepositoryImpl(
    private val service: NewsServices,
    private val articleDao: ArticleDao,
    private val articleMapper: ArticleMapper
) : NewsRepository {

    override fun getTopHeadlines(): Single<List<Article>> {
        return service.getTopHeadlines("id")
            .map { articleMapper.mapToListDomain(it.response.docs!!) }
    }

    override fun getEverything(query: String, page: Int, apiKey: String): Single<List<Article>> {
        return service.getEverything(query, page, apiKey)
            .map { articleMapper.mapToListDomain(it.response.docs!!) }
//                .doOnSuccess { articleDao.saveAllArticles(articleMapper.mapToListEntity(it)) }
    }
}