package com.paduka.uzinews.data.model

/*
 Created by Fauzi Arnami on 3/16/18.
*/

data class HeadlineModel(
    val main: String? = null
)