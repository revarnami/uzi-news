package com.paduka.uzinews.data.mapper

import com.paduka.uzinews.data.database.entity.ArticleEntity
import com.paduka.uzinews.data.model.ArticleModel
import com.paduka.uzinews.data.model.ByLineModel
import com.paduka.uzinews.data.model.HeadlineModel
import com.paduka.uzinews.data.model.MultimediaModel
import com.paduka.uzinews.domain.Article

open class ArticleMapper : BaseMapper<ArticleModel, Article> {

    override fun mapToDomain(model: ArticleModel): Article {
        val url = if (model.multimedia!!.isNotEmpty()) {
            "https://static01.nyt.com/${model.multimedia[0]!!.url}"
        } else {
            "https://eumeps.org/assets/generated/images/news-placeholder.png"
        }
        return Article(
            title = model.headline?.main ?: "",
            snippet = model.snippet ?: "",
            pubDate = model.pub_date ?: "",
            author = model.byline!!.original ?: "",
            content = model.lead_paragraph ?: "",
            url = model.web_url ?: "",
            imageUrl = url,
            webUrl = model.web_url ?: ""
        )
    }

    private fun mapToEntity(model: ArticleModel): ArticleEntity {
        val url = if (model.multimedia!!.isNotEmpty()) {
            "https://static01.nyt.com/${model.multimedia[0].url}"
        } else {
            "https://eumeps.org/assets/generated/images/news-placeholder.png"
        }
        return ArticleEntity(
            url = model.web_url ?: "",
            title = model.headline!!.main ?: "",
            author = model.byline!!.original ?: "",
            content = model.lead_paragraph ?: "",
            urlImage = url ?: "",
            snippet = model.snippet ?: "",
            pubDate = model.pub_date ?: ""
        )
    }

    fun mapToListEntity(models: List<ArticleModel>): List<ArticleEntity> {
        var listEntity = mutableListOf<ArticleEntity>()

        models.map {
            listEntity.add(mapToEntity(it))
        }

        return listEntity
    }

    override fun mapToModel(domain: Article): ArticleModel {
        val headline = HeadlineModel(domain.title)
        val byLine = ByLineModel(domain.author)
        val multimedia = MultimediaModel(domain.url)
        val listMultimedia = ArrayList<MultimediaModel>()
        listMultimedia.add(multimedia)
        return ArticleModel(
            headline = headline,
            snippet = domain.snippet,
            pub_date = domain.pubDate,
            byline = byLine,
            web_url = domain.url,
            lead_paragraph = domain.content,
            multimedia = listMultimedia
        )
    }
}