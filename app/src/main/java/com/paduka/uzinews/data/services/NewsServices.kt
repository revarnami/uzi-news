package com.paduka.uzinews.data.services

import com.paduka.uzinews.data.response.TopHeadlineResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsServices {

    @GET("top-headlines")
    fun getTopHeadlines(@Query("country") country: String): Single<TopHeadlineResponse>

    @GET("articlesearch.json")
    fun getEverything(@Query("q") query: String, @Query("page") page: Int, @Query("api-key") apiKey: String): Single<TopHeadlineResponse>
}