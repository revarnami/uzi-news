package com.paduka.uzinews.screens.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.github.ajalt.timberkt.e
import com.paduka.uzinews.BuildConfig
import com.paduka.uzinews.R
import com.paduka.uzinews.shared.PaginationScrollListener
import com.paduka.uzinews.shared.view.ArticleItemView
import com.paduka.uzinews.shared.view.LoadmoreItemView
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.activity_home.*
import org.koin.android.ext.android.inject

class HomeActivity : AppCompatActivity() {

    private val homeAdapter = GroupAdapter<ViewHolder>()

    private val vm: HomeVM by inject()

    private var page = 1
    private var isLoadMore = false
    private var isLastPage = false

    private var loadmoreItemView = LoadmoreItemView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val gridLayoutManager = GridLayoutManager(this, this.resources.getInteger(R.integer.news_grid_column))

        rvHome.apply {
            layoutManager = gridLayoutManager
            adapter = homeAdapter
        }

        var query = "business"

        vm.homeState.observe(this, stateObserver)
        vm.getEverything(query = query, page = page, apiKey = BuildConfig.API_KEY)

        rvHome.addOnScrollListener(object : PaginationScrollListener(gridLayoutManager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return this@HomeActivity.isLoadMore
            }

            override fun loadMoreItems() {
                isLoadMore = true
                page++

                e { "loadmore page $page" }

                vm.getEverything(query, page, BuildConfig.API_KEY)
            }
        })
    }

    private val stateObserver = Observer<HomeScreenState> { state ->
        when (state) {
            is LoadingState -> {
                if (isLoadMore) {
                    // add loading indicator
                    homeAdapter.add(loadmoreItemView)
                } else {
                    // todo: setup loading state
                }
            }
            is ErrorState -> {
            }
            is ArticleLoadedState -> {
                if (isLoadMore) {
                    // remove loading indicator
                    homeAdapter.remove(loadmoreItemView)
                    isLoadMore = false
                }

                state.articles.map {
                    homeAdapter.add(ArticleItemView(it, this))
                }
            }
        }
    }
}
