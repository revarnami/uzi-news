package com.paduka.uzinews.screens.home

import androidx.lifecycle.MutableLiveData
import com.github.ajalt.timberkt.d
import com.github.ajalt.timberkt.e
import com.paduka.uzinews.data.repository.NewsRepository
import com.paduka.uzinews.domain.Article
import com.paduka.uzinews.screens.base.BaseViewModel
import com.paduka.uzinews.shared.RxUtils

sealed class HomeScreenState
object LoadingState : HomeScreenState()
data class ErrorState(var msg: String?) : HomeScreenState()
data class ArticleLoadedState(val articles: List<Article>) : HomeScreenState()

class HomeVM(val repo: NewsRepository) : BaseViewModel() {

    var homeState = MutableLiveData<HomeScreenState>()
    var listArticle = mutableListOf<Article>()

    fun getTopHeadlines() {
        homeState.value = LoadingState

        compositeDisposable.add(
            repo.getTopHeadlines()
                .compose(RxUtils.applySingleAsync())
                .subscribe({ result ->
                    d { "result size ${result.size}" }
                    homeState.value = ArticleLoadedState(result)
                }, this::onError)
        )

    }

    fun getEverything(query: String, page: Int, apiKey: String) {
        homeState.value = LoadingState

        compositeDisposable.add(
            repo.getEverything(query = query, page = page, apiKey = apiKey)
                .compose(RxUtils.applySingleAsync())
                .subscribe({ result ->
                    //                    listArticle.addAll(result)
                    e { "article size ${result.size}" }
                    homeState.value = ArticleLoadedState(result)

                }, this::onError)
        )
    }

    override fun onError(error: Throwable) {
        e { "error ${error.localizedMessage}" }
        homeState.value = ErrorState(error.localizedMessage)
    }
}