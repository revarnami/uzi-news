package com.paduka.uzinews.screens.news_detail

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.paduka.uzinews.R
import com.paduka.uzinews.navigation.navigateBackToNewsDetail
import com.paduka.uzinews.shared.CommonHelper
import com.paduka.uzinews.shared.Extras
import kotlinx.android.synthetic.main.activity_news_detail.*

class NewsDetailActivity : AppCompatActivity() {
    lateinit var webUrl: String
    private lateinit var loadingDialog: AlertDialog
    private var shouldOverrideUrl: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)
        webUrl = intent.getStringExtra(Extras.web_url)

        loadingDialog =
            CommonHelper.getDialogProgressBarBuilder(this)
                .create()

        iv_back_news_detail.setOnClickListener {
            navigateBackToNewsDetail(this)
        }

        setting()
        loadWebsite()
    }

    @Suppress("DEPRECATION")
    @SuppressLint("SetJavaScriptEnabled")
    private fun setting() {
        val web_settings: WebSettings = news_detail_wv.settings
        web_settings.javaScriptEnabled = true
        web_settings.allowContentAccess = true
        web_settings.loadsImagesAutomatically = true
        web_settings.cacheMode = WebSettings.LOAD_NO_CACHE
        web_settings.setRenderPriority(WebSettings.RenderPriority.HIGH)
        web_settings.setEnableSmoothTransition(true)
        web_settings.domStorageEnabled = true
        web_settings.loadWithOverviewMode = true
        web_settings.useWideViewPort = true
    }

    @SuppressLint("ObsoleteSdkInt")
    private fun loadWebsite() {
        if (Build.VERSION.SDK_INT >= 19) {
            news_detail_wv.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        } else {
            news_detail_wv.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }


        news_detail_wv.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
            }
        }

        news_detail_wv.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                Log.e("TAG", "shouldOverrideUrlLoading: url = ${request!!.url.toString()}")
                shouldOverrideUrl = request!!.url.toString()
                loadingDialog.show()
                news_detail_wv.loadUrl(shouldOverrideUrl)
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                if (shouldOverrideUrl == url) {
                    loadingDialog.dismiss()
                }
            }

            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                super.onReceivedError(view, request, error)
            }
        }

        news_detail_wv.loadUrl(webUrl)
    }
}
