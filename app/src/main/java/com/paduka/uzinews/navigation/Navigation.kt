package com.paduka.uzinews.navigation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.paduka.uzinews.R.anim.*
import com.paduka.uzinews.screens.home.HomeActivity
import com.paduka.uzinews.screens.news_detail.NewsDetailActivity

fun navigateToNewsDetail(context: Context, bundle: Bundle) {
    if (context != null && context is Activity) {
        val activity = context
        val flags = context.flags(Intent.FLAG_ACTIVITY_NEW_TASK, Intent.FLAG_ACTIVITY_CLEAR_TOP)
        activity.start<NewsDetailActivity>(bundle, flags, enter_from_right, exit_to_left)
    }
}

fun navigateBackToNewsDetail(context: Context) {
    if (context != null && context is Activity) {
        val activity = context
        val flags = context.flags(Intent.FLAG_ACTIVITY_NEW_TASK, Intent.FLAG_ACTIVITY_CLEAR_TOP)
        activity.start<HomeActivity>(flags, enter_from_left, exit_to_right)
    }
}





