package com.paduka.uzinews.di

import androidx.room.Room
import com.google.gson.GsonBuilder
import com.paduka.uzinews.data.database.AppDatabase
import com.paduka.uzinews.data.mapper.ArticleMapper
import com.paduka.uzinews.data.repository.NewsRepository
import com.paduka.uzinews.data.repository.NewsRepositoryImpl
import com.paduka.uzinews.data.services.NewsInterceptor
import com.paduka.uzinews.data.services.NewsServices
import com.paduka.uzinews.screens.home.HomeVM
import com.paduka.uzinews.shared.DBNameSpace
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val appModule = module {
    single { NewsInterceptor() }
    single { createOkHttpClient(get()) }
    single { createWebService<NewsServices>(get(), "https://api.nytimes.com/svc/search/v2/") }

    single {
        Room.databaseBuilder(androidApplication(), AppDatabase::class.java, DBNameSpace.DATABASE_NAME).build()
    }
}

val dataModule = module {

    // db dao
    single { get<AppDatabase>().articleDao() }

    // repository
    single { NewsRepositoryImpl(get(), get(), get()) as NewsRepository }

    // mapper
    single { ArticleMapper() }

    // viewmodel
    viewModel { HomeVM(get()) }
}

fun createOkHttpClient(interceptor: NewsInterceptor): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    val timeout = 60L
    return OkHttpClient.Builder()
        .connectTimeout(timeout, TimeUnit.SECONDS)
        .readTimeout(timeout, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .addInterceptor(interceptor)
        .build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
    val gson = GsonBuilder()
        .setDateFormat("yyyy-MM-dd HH:mm:ss")
        .create()

    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    return retrofit.create(T::class.java)
}

val myAppModule = listOf(appModule, dataModule)