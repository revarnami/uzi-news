package com.paduka.uzinews.shared

/*
 Created by Fauzi Arnami on 3/16/18.
*/

object DBNameSpace {
    const val DATABASE_NAME = "db_uzi_news"
}

object Extras {
    const val web_url = "web_url"
}