package com.paduka.uzinews.shared.view

import android.content.Context
import android.os.Bundle
import com.paduka.uzinews.R
import com.paduka.uzinews.domain.Article
import com.paduka.uzinews.navigation.navigateToNewsDetail
import com.paduka.uzinews.shared.Extras
import com.paduka.uzinews.shared.extensions.loadSrc
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.layout_item_article.view.*

class ArticleItemView(private val article: Article, private val context: Context) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        val imgArticle = viewHolder.itemView.imgArticle
        val titleArticle = viewHolder.itemView.titleArticle
        val snippetArticle = viewHolder.itemView.snippetArticle
        val pubDateArticle = viewHolder.itemView.pubDateArticle

        imgArticle.loadSrc(article.imageUrl)
        titleArticle.text = article.title
        snippetArticle.text = article.snippet
        pubDateArticle.text = article.pubDate

        viewHolder.itemView.setOnClickListener {
            val bundle = Bundle()
            bundle.putString(Extras.web_url, article.webUrl)
            navigateToNewsDetail(context, bundle)
        }
    }

    override fun getLayout(): Int = R.layout.layout_item_article
}