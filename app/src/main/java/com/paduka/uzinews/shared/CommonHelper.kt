package com.paduka.uzinews.shared

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.paduka.uzinews.R

object CommonHelper {
    fun getDialogProgressBarBuilder(
        context: Context
    ): AlertDialog.Builder {
        val builder: AlertDialog.Builder
        builder = AlertDialog.Builder(context)
        builder.setView(R.layout.layout_loading_data_dialog)
        builder.setCancelable(false)
        return builder
    }
}